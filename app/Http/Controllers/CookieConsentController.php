<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CookieConsentController extends Controller
{
    public function show(){
        return view('cookie-consent');
    }

    public function store(Request $request){

        $consent = [
            'necessary' => true,
            'preferences' => $request->input('accept_all') === 'true' || $request->input('preferences') === 'on',
            'analytics' => $request->input('accept_all') === 'true' || $request->input('analytics') === 'on',
            'marketing' => $request->input('accept_all') === 'true' || $request->input('marketing') === 'on',
        ];

        // Save consent as a JSON encoded cookie
        Cookie::queue('cookie_consent', json_encode($consent), 60 * 24 * 365); // 1 year

        return response()->json(['status' => 'success']);

    }


}
