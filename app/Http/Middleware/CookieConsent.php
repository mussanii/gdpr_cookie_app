<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CookieConsent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $cookieConsent = Cookie::get('cookie_consent');

        if($cookieConsent){
            //Decode the JSON consent data
            $consent = json_decode($cookieConsent, true);

            //set cookies based on user preferences
            if (!$consent['analytics']) {
                Cookie::queue(Cookie::forget('analytics_cookie',true));
            }

            if(!$consent['marketing']){
                Cookie::queue(Cookie::forget('marketing_cookie',true));

            }
            if(!$consent['preferences']){
                Cookie::queue(Cookie::forget('preferences_cookie',true));

            }


        }else{
            //Ensure no cookies are set until consent is given
            Cookie::queue(Cookie::forget('marketing_cookie'));
            Cookie::queue(Cookie::forget('preferences_cookie'));
            Cookie::queue(Cookie::forget('analytics_cookie'));

        }


        return $next($request);
    }
}
