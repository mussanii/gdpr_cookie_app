<?php

use App\Http\Controllers\CookieConsentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cookie-consent', [CookieConsentController::class, 'show'])->name('cookie-consent.show');
Route::post('/cookie-consent', [CookieConsentController::class, 'store'])->name('cookie-consent.store');
