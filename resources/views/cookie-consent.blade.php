<!DOCTYPE html>
<html>
<head>
    <title>Cookie Consent</title>
    <style>
        #cookie-consent-banner {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            background-color: #2c3e50;
            color: #ecf0f1;
            padding: 20px;
            box-shadow: 0 -2px 10px rgba(0, 0, 0, 0.2);
            z-index: 1000;
        }
        #cookie-consent-banner p {
            margin: 0 0 10px 0;
        }
        #cookie-consent-banner button {
            background-color: #3498db;
            border: none;
            color: #fff;
            padding: 10px 20px;
            margin-right: 10px;
            cursor: pointer;
            border-radius: 5px;
        }
        #cookie-consent-banner button:hover {
            background-color: #2980b9;
        }
        #cookie-settings {
            display: none;
        }
        #cookie-settings label {
            display: block;
            margin: 10px 0;
        }
    </style>
</head>
<body>
    <div id="cookie-consent-banner">
        <form id="cookie-consent-form" action="{{ route('cookie-consent.store') }}" method="POST">
            @csrf
            <p>This site uses cookies to ensure that we give you the best experience on the website. This includes cookies that are essential to enable you to navigate the website and get the most out of its features. Without these cookies, core services on the eLearning program such as login,registration and exploring courses cannot be provided. You can set your browser to block or alert you about these cookies, but please note that that some parts of the site might not work if cookies are disabled. By continuing to browse the site, you are agreeing to our use of cookies.</p>
            <div id="cookie-settings">
                <div>
                    <label>
                        <input type="checkbox" name="necessary" checked disabled> Strictly Necessary Cookies: These cookies are necessary to make the site work properly and are always set when you visit the site.
                    </label>
                </div>
                <div>
                    <label>
                        <input type="checkbox" name="analytics"> Analytics Cookies: These cookies collect information to help us understand how the site is being used.
                    </label>
                </div>
                <div>
                    <label>
                        <input type="checkbox" name="marketing"> Marketing Cookies: These cookies are used to make advertising messages more relevant to you. In some cases, they also deliver additional functions to the site.
                    </label>
                </div>
            </div>
            <div style="justify-content: center;">
            <button type="button" id="accept-all">Accept All Cookies</button>
            <button type="button" id="customize-settings">Customize Settings</button>
            <button type="button" ><a href="/cookie-acceptance">Learn More</a></button>
            <button type="submit" style="display: none;" id="save-preferences">Save Preferences</button>
            </div>
           
        </form>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const cookieConsentBanner = document.getElementById('cookie-consent-banner');
            const cookieSettings = document.getElementById('cookie-settings');
            const acceptAllButton = document.getElementById('accept-all');
            const customizeSettingsButton = document.getElementById('customize-settings');
            const savePreferencesButton = document.getElementById('save-preferences');

            // Check if consent is already given
            const cookieConsent = document.cookie.split('; ').find(row => row.startsWith('cookie_consent'));
            if (!cookieConsent) {
                cookieConsentBanner.style.display = 'block';
            }

            acceptAllButton.addEventListener('click', function() {
                const formData = new FormData();
                formData.append('accept_all', 'true');

                fetch('{{ route('cookie-consent.store') }}', {
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(Object.fromEntries(formData))
                }).then(response => response.json())
                  .then(result => {
                      cookieConsentBanner.style.display = 'none';
                  });
            });

            customizeSettingsButton.addEventListener('click', function() {
                cookieSettings.style.display = 'block';
                savePreferencesButton.style.display = 'inline';
                acceptAllButton.style.display = 'none';
                customizeSettingsButton.style.display = 'none';
            });

            const form = document.getElementById('cookie-consent-form');
            form.addEventListener('submit', function(e) {
                e.preventDefault();

                const formData = new FormData(form);
                const data = {
                    analytics: formData.get('analytics') === 'on',
                    marketing: formData.get('marketing') === 'on'
                };

                fetch(form.action, {
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }).then(response => response.json())
                  .then(result => {
                      cookieConsentBanner.style.display = 'none';
                  });
            });
        });
    </script>
</body>
</html>
