<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Laravel
            </div>

            <div>
                <a href="{{ route('cookie-consent.show') }}">Manage Cookie Preferences</a>
            </div>
        </div>
    </div>

    <script>
        // Ensure the cookie consent banner is displayed if no consent is given
        document.addEventListener('DOMContentLoaded', function () {
            const cookieConsent = document.cookie.split('; ').find(row => row.startsWith('cookie_consent'));
            if (!cookieConsent) {
                window.location.href = "{{ route('cookie-consent.show') }}";
            }
        });
    </script>
</body>
</html>
